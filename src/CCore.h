#pragma once
#include <SDL2/SDL.h> 
#include <SDL2/SDL_ttf.h>
#include <vector>  
#include <time.h> 
#include <string>
#include <fstream>
#include <iostream>
#include "location.h"
#include "CCat_Driver.h"
#include "constants.h"
#include "location.h"
#include "CStone.h"
#include "CAir.h"
#include "CMud.h"
#include "CPosion.h"
#include "CChees.h"
#include "CGrass.h"
#include "CDriver.h"
#include "CB_Mouse.h"
#include "CB_Cat.h"
#include "CB_Spider.h"
#include "CMouse_Driver.h"
#include "CGrafic.h"
#include "CGenerator_moves.h"
#include "CCat_generator_moves.h"
/** Class represent main core of game, creat player, enemy, draw scene and  control input */
class CCore
{
public:
    /** 
    *@throw const char* throw lib error
    *@throw const char* throw setting error 
    */
	CCore();
			/**
  * Methods run uppate scene, draw scene to screen and control frame rate.
   */
	void main_loop();
		/**
  * Methods redraw all drivers and take input from keyboard and genetarots of moves.
   */
	void update_scene();
	/**
  * Methods creat player (mouse) and driver.
   */
	void add_player();
	/**
  * Methods add enemy to vector of enemis ( 50% cat 50% spider).
   */
	void add_enemy();
	/**
  * Methods add spider to vector of enemies.
   */
	void add_enemy_spider();
		/**
  * Methods add cat to vector of enemies.
   */
	void add_enemy_cat();
	/**
  * Methods load map from file and creat player and enemies from file.
  * 
  * @param[in] name of file with map
   */
	void load_map( const char * file_name );
	 	/**
  * Methods creat window and welocom screen
   *@throw const char* throw map load error
   * @param[in] name of file with map
   */
	void init(const char * file_name = NULL );
 	/**
  * Methods take input from keyboar and drive object drive by input.
  * Control is space put object
  * Left ctrl punch
  * left arrow move left
  * right arrow move right
  * up arrow move up
  * down arrow move down
  * s save game
  * @param[in] driver of object to drive
   */
	void input_control( CMouse_Driver &object_to_control);
	~CCore();
private:
	CB_Mouse * mouse;
	CB_Cat * cat;
	CCat_Driver * cat_driver;
	CCat_generator_moves * generator;
	CMouse_Driver * mouse_driver;
	CGrafic grafic;
	CMAP * Map;
	bool program_run;
	const Uint8 *state;
	SDL_Event * event;
	CSurface container_of_surface;
	std::vector<std::pair< CGenerator_moves *, CDriver * >> enemies;
};
