#pragma once
#include <string>
#include "CMAP.h"
#include <SDL2/SDL.h> 
#include "CLive_Object.h"
#include "CMAP.h"
#include "location.h"
#include <vector>
/** Class represents abstract drivet for live_objects, drive objects and coordinate their moves and properties with map */
class CDriver{
	public:
	 /** Constructor of CDdriver setup driver for first use
	  * @param[in,out] Map for moving object
 	  * @param[in,out] object_to_control this object is driven by Driver
      */
		CDriver( CMAP * Map, CLive_Object * object_to_control );
		 /** Constructor of CDdriver setup driver for first use
		 */
		CDriver();
		 /** Destruktor of driver
		 */
		virtual ~CDriver();
		 /*	* Method move object left x + STANDART_O_WIDE  
		 */
		virtual void move_left();
		 /** Method move object left x - STANDART_O_WIDE 
		 */
		virtual void move_right();
		 /** Method move object left y - STANDART_O_HEIGHT 
		 */
		virtual void  move_down();
		 /*	* Method move object left y + STANDART_O_HEIGHT 
		 */
		virtual void move_up();
		/** Method change type of object, object of this type will be put on map when space is insert
		 */
		virtual void change_type_of_put( int code_of_object );
		/*	* Method reset timer, timer is use for waitind method
		 */
		virtual void reset_timer();
			/**
  			* Method draw driven object to renderer.
  			@param[in,out] renderer where will be draw object.
  			*/
		virtual void draw_me( SDL_Renderer * renderer );
		/** Method recognize if object is dead or not 
		* @return false if lives > 0 if lives = 0 true
		 */
		virtual bool is_dead();
		
	protected:
		/** Method signal if time limit is reach
		* @param[in]  ms how long will method return false 
		* @return return true if time limit is reach if not false
		 */
		virtual bool wait( unsigned int ms );
		/**
 		 * Method is call before live_object is move. In this metod should be called other method like attack... 
   		*/
		virtual void spec_funtion_for_type( const std::vector<location> &loc );
		/**
 		 * Method result what do with object on loc
 		 * @param[in] loc location for work 
   		*/
		virtual void eat_cube( const std::vector<location> &loc );
		/**
 		 * Method move down object if under object is free space (AIR )
  		* @return hight of fall 
   		*/
		virtual int gravitate();
		/** Method attack on object on location if there is object to kill  
		* @param[in] location where attack object
		*/
		virtual void attack( const std::vector<location> &loc );
		int side;
		bool is_waiting; 
		unsigned int time_by_wait;
		int  m_code_of_object;
		CMAP * m_Map;
		CLive_Object * m_object_to_control;
		bool reset_t;
};

