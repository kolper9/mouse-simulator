#pragma once
#include <string>
#include <SDL2/SDL.h> 
#include "CDriver.h"
#include <vector>
/** Class represents  driver for cat, drive objects and coordinate their moves and properties with map */
class CCat_Driver : public CDriver 
{
	public:
	 /** Constructor of CDdriver setup driver for first use
	  * @param[in,out] Map for moving object
 	  * @param[in,out] object_to_control this object is driven by Driver
      */
		CCat_Driver( CMAP * Map, CLive_Object * object_to_control  );

		/**
 		 * Method is call before live_object is move. In this metod should be called other method like attack... 
   		*/
		virtual void spec_funtion_for_type( const std::vector<location> &loc );
		/**
 		 * Method result what do with object on loc
 		 * @param[in] location for work 
   		*/
		virtual void eat_cube( const std::vector<location> &loc );
		~CCat_Driver();
};

