
#include <string>
#include "CMAP.h"
#include <SDL2/SDL.h> 
#include "CLive_Object.h"
#include "location.h"
#include "CObject.h"
#include "CStone.h"
#include "CAir.h"
#include "CMud.h"
#include "CPosion.h"
#include "CChees.h"
#include "CGrass.h"

void CMAP::draw_me (const location &loc){
	if(to_positiv_x( loc.m_x ) && to_positiv_y( loc.m_y)){
		matrix[loc.m_x/STANDART_O_WIDE][loc.m_y/STANDART_O_HEIGHT]->draw_me(m_renderer);
	}
}
bool CMAP::to_positiv_x ( const int x) const {
	if(x<0){
		return false;	
	}
	if(x>=WIN_WIDE-1){
		return false;	
	}
	return true;
}

bool CMAP::to_positiv_y ( const int x) const{
	if(x<0){
		return false;	
	}
	if(x>=WIN_HEIGHT-1){
		return false;	
	}
	return true;
}

//-------------------------------------------------------------------------------------------------------------------------
CMAP::CMAP( SDL_Renderer * renderer){
	is_fill=false;
	matrix= new  CObject**[WIN_WIDE/STANDART_O_WIDE];
	for( int i=0; i<WIN_WIDE/STANDART_O_WIDE; i++ ){
		matrix[i]=new CObject *[WIN_HEIGHT/STANDART_O_HEIGHT];
	}
	m_renderer=renderer;
}
//-------------------------------------------------------------------------------------------------------------------------
bool CMAP::can_go_trought( const std::vector<location> &loc, const int setup ) const{
	for ( auto  i =loc.begin(); i!=loc.end(); ++i ){
		if(setup==0){
			if(to_positiv_x( i->m_x) && to_positiv_y( i->m_y) ){
				if(!matrix[  i->m_x/STANDART_O_WIDE][i->m_y/STANDART_O_HEIGHT]->can_go_trought()){
					return false;
				}
			}
			
		}
		if(setup==1){
			if(to_positiv_x( i->m_x) && to_positiv_y( i->m_y) ){
				if(! matrix[ i->m_x/STANDART_O_WIDE ][ i->m_y/STANDART_O_HEIGHT]->free_move()){
					return false;
				}
			}
		
		}
	}
	return true;
}
//-------------------------------------------------------------------------------------------------------------------------
CObject * CMAP::return_position( const location &loc) const{
	return matrix[loc.m_x/STANDART_O_WIDE][loc.m_y/STANDART_O_HEIGHT];
}
//-------------------------------------------------------------------------------------------------------------------------
void CMAP::change_on_air( const location &loc ){
	if(matrix[loc.m_x/STANDART_O_WIDE][loc.m_y/STANDART_O_HEIGHT]->deletable()){
		delete matrix[loc.m_x/STANDART_O_WIDE][loc.m_y/STANDART_O_HEIGHT];
	}
	matrix[loc.m_x/STANDART_O_WIDE][loc.m_y/STANDART_O_HEIGHT]=new CAir( loc.m_x,loc.m_y,container_of_surface.return_surface_of(AIR) );
}
//-------------------------------------------------------------------------------------------------------------------------
void CMAP::change_on_mud( const location &loc ){
	if( matrix[loc.m_x/STANDART_O_WIDE][loc.m_y/STANDART_O_HEIGHT]->deletable() ){
		delete matrix[loc.m_x/STANDART_O_WIDE][loc.m_y/STANDART_O_HEIGHT];
	}
	matrix[loc.m_x/STANDART_O_WIDE][loc.m_y/STANDART_O_HEIGHT]=new CMud( loc.m_x,loc.m_y,container_of_surface.return_surface_of(MUD) );
}
//-------------------------------------------------------------------------------------------------------------------------
CMAP::~CMAP(){
	if( is_fill ){
		for( int i=0;i<WIN_WIDE;i+=STANDART_O_WIDE ){
			for ( int k=0; k<WIN_HEIGHT; k+=STANDART_O_HEIGHT ){
				if( matrix[i/STANDART_O_WIDE][k/STANDART_O_HEIGHT]->deletable() ){
					delete matrix[i/STANDART_O_WIDE][k/STANDART_O_HEIGHT];
				}
			}
		}

	}
		for( int i=0; i<WIN_WIDE/STANDART_O_HEIGHT;i++ ){
		delete[] matrix[i];
	}
	delete[] matrix;
}
//--------------------------------------------------------------------------------------------------------
void CMAP::change_on( const std::vector<location>  &loc, CObject * changing_on){
	for ( auto  i =loc.begin(); i !=loc.end(); ++i ){
		if( matrix[i->m_x/STANDART_O_WIDE][i->m_y/STANDART_O_HEIGHT]->deletable() ){
			delete matrix[i->m_x/STANDART_O_WIDE][i->m_y/STANDART_O_HEIGHT];
		}	
		matrix[i->m_x/STANDART_O_WIDE][i->m_y/STANDART_O_HEIGHT]=changing_on;
	}
}
void CMAP::change_on( const location  &loc, CObject * changing_on){
		matrix[loc.m_x/STANDART_O_WIDE][loc.m_y/STANDART_O_HEIGHT]=changing_on;
}
//--------------------------------------------------------------------------------------------------------
void CMAP::re_draw(const std::vector<location>  &loc,int  &code_of_object){
	for (auto  i =loc.begin(); i !=loc.end(); ++i){
				if(code_of_object==AIR){
					change_on_air(*i);
				}
				if(code_of_object==MUD){
					change_on_mud(*i);
				}
				return_position(*i)->draw_me( m_renderer);
				code_of_object=AIR;
	}
}
//--------------------------------------------------------------------------------------------------------
void CMAP::first_use(    ){
	is_fill=true;
		for(int i=0;i<WIN_WIDE;i+=STANDART_O_WIDE){
				for (int k = 0; k < GROUND; k+=STANDART_O_HEIGHT){
					matrix[i/STANDART_O_WIDE][k/STANDART_O_HEIGHT]= new CAir(i,k,container_of_surface.return_surface_of(AIR));
					matrix[i/STANDART_O_WIDE][k/STANDART_O_HEIGHT]->draw_me(m_renderer);
				}
		}
		for(int i=0;i<WIN_WIDE;i+=STANDART_O_WIDE){
				int k =  GROUND; 
					matrix[i/STANDART_O_WIDE][k/STANDART_O_HEIGHT]= new CGrass(i,k,container_of_surface.return_surface_of(GRASS));
					matrix[i/STANDART_O_WIDE][k/STANDART_O_HEIGHT]->draw_me(m_renderer);	
			}
		for(int i=0;i<WIN_WIDE;i+=STANDART_O_WIDE){
			for (int k = GROUND+STANDART_O_HEIGHT; k < WIN_HEIGHT; k+=STANDART_O_HEIGHT){
				if(rand()%3==1){
					if(rand()%5==1){
						if(rand()%2==1){
							matrix[i/STANDART_O_WIDE][k/STANDART_O_HEIGHT]= new CChees(i,k,container_of_surface.return_surface_of(CHEES));
						}
						else{
							matrix[i/STANDART_O_WIDE][k/STANDART_O_HEIGHT]= new CPosion(i,k,container_of_surface.return_surface_of(POSION));
						}
					}
					else{
						matrix[i/STANDART_O_WIDE][k/STANDART_O_HEIGHT]= new CStone(i,k,container_of_surface.return_surface_of(ROCK));
					}
				}
				else{
				matrix[i/STANDART_O_WIDE][k/STANDART_O_HEIGHT]= new CMud(i,k,container_of_surface.return_surface_of(MUD));
				}	
				matrix[i/STANDART_O_WIDE][k/STANDART_O_HEIGHT]->draw_me(m_renderer);
			}
		}
}
int  CMAP::health( const std::vector<location> &loc  ) const {
	int lives=0;
		for ( auto  i =loc.begin(); i !=loc.end(); ++i ){
			lives+=matrix[i->m_x/STANDART_O_WIDE][i->m_y/STANDART_O_HEIGHT]->live_change();
		}		
	return lives;
}

CObject *  CMAP::to_kill( const std::vector<location> &loc  ) const {

	for ( auto  i =loc.begin(); i !=loc.end(); ++i ){
		if(to_positiv_x( i->m_x) && to_positiv_y( i->m_y) ){
			if(matrix[  i->m_x/STANDART_O_WIDE][ i->m_y/STANDART_O_HEIGHT]->to_kill()){
				return matrix[  i->m_x/STANDART_O_WIDE][ i->m_y/STANDART_O_HEIGHT];
			}
		}
	
	}		
	return NULL;
}

std::string CMAP::save(){
	std::string save_file;
	
			for ( int k=0; k<WIN_HEIGHT; k+=STANDART_O_HEIGHT ){
				for( int i=0;i<WIN_WIDE;i+=STANDART_O_WIDE ){
					if(matrix[i/STANDART_O_WIDE][k/STANDART_O_HEIGHT]->save_able()){
						save_file+=matrix[i/STANDART_O_WIDE][k/STANDART_O_HEIGHT]->save()+=",";
					}
					else{
						save_file+=std::to_string(AIR)+=",";
					}
					}
			save_file+='/';
			save_file+='\n';
		}
			
		return save_file;

}