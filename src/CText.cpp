
#include <string>
#include <SDL2/SDL.h> 
#include <SDL2/SDL_ttf.h>
#include "location.h"
#include "CText.h"
#include "constants.h"



CText::CText(){
 	color={0,0,0,255};
 	font=TTF_OpenFont(FONT, 16);
 	rect = new SDL_Rect;
	if(!font) {
	    throw "FONT ERROR";
	}
 }
void CText::show( std::string &text, SDL_Renderer * renderer, int x, int y, int wide, int height ){
	text_surface=TTF_RenderText_Solid(font,text.c_str(),color);
	rect->x=x; 
	rect->y=y; 
	rect->w=wide; 
	rect->h=height; 
	texture=SDL_CreateTextureFromSurface( renderer, text_surface );
	SDL_RenderCopy( renderer, texture, NULL, rect );
	SDL_DestroyTexture( texture );
 }

CText::~CText(){
 	if( rect ){
		delete rect;
		rect=NULL;
	}	
 }
