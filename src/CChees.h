#pragma once
#include <string>
#include "CObject.h"
#include <SDL2/SDL.h> 
/** Class CChees represent box object in game 
* Main properties: give lives
*/
class CChees : public CObject 
{
public:
	/**
	*Constructor of class 
	* @param[in] x place for spawn object
  * @param[in] y place for spawn object
  * @param[in] sufrace with object picture
  */
	CChees( int x, int y, SDL_Surface * surface );
		/**
	*Method return number of live that give this object
	@return number of live ( can be negativ )
  */
	int live_change();
		/**
  	* Method return information about object.
  	@return string type of object (3)
  	*/	
	virtual std::string save();

};
