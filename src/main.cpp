#include <fstream>
#include <string>
#include "CCore.h"

int main(int argc, char const *argv[])      
{
	try
   	{
		CCore program;
		std::string text;
		if(argc==2){
			std::ifstream f(argv[1]);
	   			if( !f.good()){
	   				std::cout<<"NO_EXIST_FILE_ERROR"<<std::endl;
	   				return 0;
	   			}
	   			f.close();
	   		
					program.init(argv[1]);
		}		
		else{
			 program.init();
		}
		program.main_loop();
	}
	catch ( char const * a ){
		std::cout<<a<<std::endl;
	}
	std::cout<<"GOODBYE"<<std::endl;
    return 0;
}


