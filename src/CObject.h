
#pragma once
#include <string>
#include <SDL2/SDL.h> 
/** Class CObject represents object in game is abstract class and all box and  live objects inherit its properties */
class CObject{
public:
	virtual ~CObject();
	/**
	 *@param[in,out] renderer where will be draw object.
 	 */
	virtual void draw_me( SDL_Renderer * renderer );
	/** Method  return true if object can free and movou trought other object
	 * @return  true if can free  go trought return true
	 */
	virtual bool deletable();
	/** Method  return true if object can free and movou trought other object
	 * @return  true if can go trought return true
	 */
	virtual bool free_move();
	/** Method  return true if object can movou trought other object
	 * @return  true if can go trought return true
	 */
	virtual bool can_go_trought();
	/** Method  return true if object can free and movou trought other object
	 * @param[in] numer experiences to add 
	 *  @return nuber of actual experiences
	 */
	virtual int live_change();
		/** Method  return tru if should be this object kill
		 *  @return true if object shoulb be kill
		 */
	virtual bool to_kill();
		/** Method  return true if object can be save  is reset after wide*height task 
		  * @return  return true if object can be save 
		 */
	virtual bool save_able();
		/** Method  return true if object can be save  is reset after wide*height task 
		  * @return  return true if object can be save 
		 */
	virtual std:: string save();
protected:
	int m_height;
	int m_wide;
	int m_x;
	int m_y;
	SDL_Surface * SurfaceWithPicture;
	SDL_Rect * rect;
	SDL_Texture * texture;
};