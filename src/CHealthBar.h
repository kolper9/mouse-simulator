#pragma once
#include "constants.h"
#include "CLive.h"
/** Class represents bar with health maximum is 3 lives */
class CHealthBar {
public:
CHealthBar();
~CHealthBar();
	/**
  	*Method draw object to renderer.
  	*@param[in,out] renderer where will be draw object.
  	*@param[in] nuber of lives to re_draw
  	*/
void re_draw(  SDL_Renderer * renderer, int lives );
private:
	CLive * first, * second, * third;
	int x=WIN_WIDE-STANDART_O_WIDE;
	int y=0;
	int m_lives;
};

