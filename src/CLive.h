#pragma once
#include "CObject.h"
/** Class represents one box in health bar,, it can be change on death or live */
class CLive: public CObject 
{
public:
	/**
	*Constructor of class 
	* @param[in] x place for spawn object
  * @param[in] y place for spawn object
  */
	CLive( int x, int y );
		/**
	*Change picture on death picture
  */
	void change_on_death();
		/**
	*Change picture on death picture
  */
	void change_on_live();
	~CLive();
};