
#include "CMAP.h"
#include <SDL2/SDL.h> 
#include "CLive_Object.h"
#include "location.h"
#include "CDriver.h"
#include "constants.h"

int CDriver::gravitate(){
	int height_of_fall=0;
	while( m_object_to_control->M_Y()<WIN_HEIGHT-m_object_to_control->height() && m_Map->can_go_trought(m_object_to_control->location_for_faling(),FREE_MOVE)){
		move_down();
		height_of_fall++;
	}
	if(height_of_fall>5){
		m_object_to_control->change_live(-1);
	}
	return height_of_fall;
}

void CDriver::attack( const std::vector<location> &loc ){
	reset_t=false;
	CObject * x = m_Map->to_kill( loc );
	if( m_object_to_control->M_X() &&
		m_object_to_control->M_X() <WIN_WIDE - m_object_to_control->wide() && 
		m_object_to_control->M_Y()<WIN_HEIGHT-m_object_to_control->height() &&
		m_object_to_control->M_Y() &&
		x  ){

			if( wait(TIME_OF_FIGHT) ){
					CLive_Object * object_to_control =  dynamic_cast <  CLive_Object*>(x);
					if(object_to_control->player()^m_object_to_control->player()){
						m_object_to_control->xpincrease(10);
						object_to_control->change_live(-1);	
					}
					reset_t=true;
			}	
	}
	else{
		reset_t=true;	
	}
}

bool CDriver::is_dead(){
	return m_object_to_control->is_kill();
}

void CDriver::draw_me( SDL_Renderer * renderer ){
	
	if(m_object_to_control->is_kill()){
		int i=AIR;
		m_object_to_control->draw_me( renderer );
		m_Map->re_draw(m_object_to_control->locations_of_object(),i);			
	}
	else{
		gravitate();
		m_object_to_control->draw_me( renderer );
	}
}
CDriver::CDriver(){
	side=RIGHT;
	reset_t=true;
}
void CDriver::eat_cube( const std::vector<location> &loc ){
	(void)loc;
	return;	
}
void CDriver::spec_funtion_for_type( const std::vector<location> &loc ){
	(void)loc;
	return;
}

void CDriver::change_type_of_put( int code_of_object ){
	m_code_of_object=code_of_object;
}
void CDriver::move_left(){
		side=LEFT;
		std::vector<location> old_loc=m_object_to_control->locations_of_object();
		spec_funtion_for_type(m_object_to_control->back());
		if(m_object_to_control->M_X() && m_Map->can_go_trought(m_object_to_control->back(),CAN_GO_THROUGT)){
			if(m_Map->can_go_trought(m_object_to_control->back(),FREE_MOVE) || wait(TIME_OF_MINING)){
				m_object_to_control->move_left();
				eat_cube( m_object_to_control->locations_of_object() );
				m_Map->re_draw(old_loc,  m_code_of_object);
		  		m_Map->change_on(m_object_to_control->locations_of_object(),m_object_to_control);
		  		gravitate();
			}
		}	
	}
	void CDriver::move_right(){
		side=RIGHT;
		std::vector<location> old_loc=m_object_to_control->locations_of_object();
		spec_funtion_for_type(m_object_to_control->front());
		if(m_object_to_control->M_X() <WIN_WIDE-m_object_to_control->wide()  && m_Map->can_go_trought(m_object_to_control->front(),CAN_GO_THROUGT)){
			if(m_Map->can_go_trought(m_object_to_control->front(),FREE_MOVE) || wait(TIME_OF_MINING)){
				m_object_to_control->move_right();
				eat_cube( m_object_to_control->locations_of_object() );
				m_Map->re_draw(old_loc,  m_code_of_object);
	  			m_Map->change_on(m_object_to_control->locations_of_object(),m_object_to_control);
	  			gravitate();
			}	
		}
		 
	}
	void CDriver::move_down(){
		std::vector<location> old_loc=m_object_to_control->locations_of_object();
		spec_funtion_for_type(m_object_to_control->under());
		if(m_object_to_control->M_Y()<WIN_HEIGHT-m_object_to_control->height() && m_Map->can_go_trought(m_object_to_control->under(),CAN_GO_THROUGT) ){
			if(m_Map->can_go_trought(m_object_to_control->under(),FREE_MOVE) ||  wait(TIME_OF_MINING)){
				m_object_to_control->move_down();
				eat_cube( m_object_to_control->locations_of_object() );
				m_Map->re_draw(old_loc,m_code_of_object);
		  		m_Map->change_on(m_object_to_control->locations_of_object(),m_object_to_control);
			}	
		}	
	}
	void CDriver::move_up(){
		std::vector<location> old_loc=m_object_to_control->locations_of_object();
		spec_funtion_for_type(m_object_to_control->top());
		if(m_object_to_control->M_Y() &&  m_Map->can_go_trought(m_object_to_control->top(),CAN_GO_THROUGT)){
			if(m_Map->can_go_trought(m_object_to_control->top(),FREE_MOVE) || wait(TIME_OF_MINING)){
				m_object_to_control->move_up();
				eat_cube( m_object_to_control->locations_of_object() );
				m_Map->re_draw(old_loc,  m_code_of_object);
				m_Map->change_on(m_object_to_control->locations_of_object(), m_object_to_control);
				gravitate();
			}	
		}	
	}
CDriver::CDriver( CMAP * Map, CLive_Object * object_to_control  ){
	m_Map=Map;
	m_object_to_control=object_to_control;
	m_code_of_object=0;
	is_waiting=false;
	time_by_wait=0;
	side=RIGHT;
	reset_t=true;
}
CDriver::~CDriver(){
	delete m_object_to_control;
}

void CDriver::reset_timer(){
	if(	reset_t ){
		is_waiting=false;
	}
}
bool CDriver::wait( unsigned int ms ){
	if(is_waiting==false){
		is_waiting=true;
		time_by_wait=SDL_GetTicks();
		return false;
	}
	if( SDL_GetTicks()-time_by_wait<ms ){			
		return false;
	}
	is_waiting=false;
	return true;
}

