#include <SDL2/SDL.h> 
#include "CSurface.h"
#include "constants.h"

CSurface::~CSurface(){
	SDL_FreeSurface( surface_of_chees);
	SDL_FreeSurface( surface_of_grass);
	SDL_FreeSurface( surface_of_posion);
	SDL_FreeSurface( surface_of_mud);
	SDL_FreeSurface( surface_of_air);
	SDL_FreeSurface( surface_of_rock);
}
//---------------------------------------------------------------------------------------------------------------------------------
CSurface::CSurface(){
	surface_of_chees=SDL_LoadBMP(CHEES_IMG);
	surface_of_grass=SDL_LoadBMP(GRASS_IMG);
	surface_of_posion=SDL_LoadBMP(POSION_IMG);
	surface_of_mud=SDL_LoadBMP(MUD_IMG);
	surface_of_air=SDL_LoadBMP(AIR_IMG);
	surface_of_rock=SDL_LoadBMP(ROCK_IMG);
	if( NULL==surface_of_chees || surface_of_grass==NULL || surface_of_posion==NULL || surface_of_mud==NULL || surface_of_air==NULL || surface_of_rock==NULL ){
		throw "OPEN IMG ERROR";
	}
}
//--------------------------------------------------------------------------------------------------------------------------------
SDL_Surface *  CSurface::return_surface_of( const int code_of_object) const {
	switch (code_of_object){
		case AIR: return surface_of_air;break;
		case MUD: return surface_of_mud;break;
		case CHEES: return surface_of_chees;break;
		case ROCK: return surface_of_rock;break;
		case GRASS: return surface_of_grass; break;
		case POSION: return surface_of_posion; break;
		default: return NULL;
	}
}
