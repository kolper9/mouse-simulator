#pragma once 
#include "CGenerator_moves.h"
#include "CDriver.h"
/** Class represent generator of enemy spider, can send signal to driver */
class CSpider_generator_moves : public CGenerator_moves{

	public:
			/**
	  *Methods creat generator 
	  *@param[in,out] driver
	   */
		CSpider_generator_moves( CDriver * driver);
		/**
	  * Methods generate move and control driver.
	   */
		void generate_move();	
};


