#pragma once
#include <string>
#include "CObject.h"
#include <SDL2/SDL.h> 
#include <vector> 
#include "location.h"
/** This class is abstract class for object that can move (player, enemy..) is control by driver */
class CLive_Object : public CObject {

	public:	
		CLive_Object();
		/** Method move object left x + STANDART_O_WIDE  
		 */
		virtual void move_left();
		/** Method move object left x - STANDART_O_WIDE 
		 */
		virtual void move_right();
		 /** Method move object left y - STANDART_O_HEIGHT 
		 */
		virtual void move_down();
		 /** Method move object left y + STANDART_O_HEIGHT 
		 */
		virtual void move_up();
		 /** Method retun x coordinates of object
		  * @return x coordinates
		 */
		virtual int M_X();
		/** Method retun y coordinates of object
		  * @return y coordinates
		 */
		virtual int M_Y();
		/** Method retun height of object
		  * @return height 
		 */
		virtual int height();
		/** Method retun wide of object
		  * @return wide 
		 */
		virtual int wide();
		/** Method  return vector of location top of object
		  * @return  vector of location 
		 */
		virtual std::vector<location> top() const;
		/** Method  return vector of location front of object
		  * @return  vector of location 
		 */
		virtual std::vector<location> front() const;
		/** Method  return vector of location back of object
		  * @return  vector of location 
		 */
		virtual std::vector<location> back() const;
		/** Method  return vector of location under of object
		  * @return  vector of location 
		 */
		virtual std::vector<location> under() const;
		/** Method  return vector of location location_for_faling of object ( object can fall down is  free move on this location )
		  * @return  vector of location 
		 */
		virtual std::vector<location> location_for_faling() const;
		/** Method  return vector of location location_for_faling of object ( object )
		  * @return  vector of location 
		 */
		virtual std::vector<location> locations_of_object() const;
		/** Method  return true if object can movou trought other object
		  * @return  true if can go trought return true
		 */
		virtual bool can_go_trought();
		/** Method  return true if object can free and movou trought other object
		  * @return  true if can go trought return true
		 */
		virtual bool free_move();
		/** Method  return true if object can be delate ( is not live object ) and replace 
		  * @return  true if can free  go trought return true
		 */
		virtual bool deletable();
			/**
  			* @return true if object can be killed if not false
  			*/
		virtual bool is_kill();
		/** Method  add lives to oject lives and return number of lives
		 * @param[in] numer lives to add 
		  * @return nuber of actual lives
		 */
		virtual int change_live( int lives );
		/** Method  return true if object can free and movou trought other object
		 * @param[in] numer experiences to add 
		  *  @return nuber of actual experiences
		 */
		virtual int xpincrease( int xp );
		/** Method  return true if object can be save  is reset after wide*height task 
		  * @return  return true if object can be save 
		 */
		virtual bool save_able();
		/** Method  return true if object is player
		  * @return  return true if object is player
		 */
		virtual bool player()=0;
			
	protected:
		bool save_number;
		int id;
		int m_lives; 	  	
		bool killed=false;
		int m_experiens;

};


