
#pragma once
#include <SDL2/SDL.h> 
#include "CObject.h"
#include "location.h"
#include <vector>
#include "CSurface.h"
#include "constants.h"
/** Class CMAP represents map of game. There are stored all object in game ( box, enemies, player). Class methods can find out thier properties and use them  */
class CMAP {
public:
	~CMAP();
	/**
	*@parametr[in,out] give parametr to renderer
	*/
	CMAP( SDL_Renderer * renderer );
	/**
	* This method generate random map
	*/
	void first_use( );
	/**
	* Method looked if there is object with atribute same as setup FREE_MOVE 1 CAN_GO_THROUGT 0
	*@parametr[in] loc location where is looked if there is object with atribute same as setup 
	*@parametr[in] setup atribute of object 
	*@return depend on setup 
	*/
	bool can_go_trought( const std::vector<location>  &loc, const int setup ) const;
	/**
	* Method change object on position with check if is on this position other object
	*@parametr[in] loc this location will be change 
	*@parametr[in,out] changing_on change on this object 
	*/
	void change_on( const std::vector<location>  &loc, CObject * changing_on );
	/**
	* Method change object on position without check if is on this position other object
	*@parametr[in] loc this location will be change 
	*@parametr[in,out] changing_on change on this object 
	*/
	void change_on( const location &loc, CObject * changing_on );
	/**
	* Method change object on position without check if is on this position other object
	*@parametr[in] loc this location will be change 
	*@parametr[in,out] changing_on change on this object 
	*/
	void re_draw(const std::vector<location>  &loc,int  &code_of_object);
	/**
	* Method collect health point from all object on location
	*@parametr[in] loc this location will be change 
	*@return number of lives
	*/
	int health( const std::vector<location> &loc ) const;
	/**
	* Method check if is on this location some object to kill and if it return first of them 
	*@parametr[in] loc this location will be change 
	*@return object to kill if is found if not NULL
	*/
	CObject *  to_kill( const std::vector<location> &loc ) const;
	/**
  	* Method call  re_draw object on location
  	@param[in] loc of object to draw
  	*/
	void draw_me (const location &loc);
	/**
  	* Method save all object in map 
  	*@return string with all object od map
  	*/
	std::string save();
private:
	/**
	* Method check if x coordinate is in map
	* @parametr[in] x coordinate
	* @return true if is in map
	*/
	bool to_positiv_x ( const int x ) const;
		/**
	* Method check if y coordinate is in map
	* @parametr[in] x coordinate
	* @return true if is in map
	*/
	bool to_positiv_y ( const int x) const;
		/**
	* Method return pointer to object on location
	* @parametr[in] loc location of object
	* @return CObject pointer
	*/
	CObject * return_position( const location &loc ) const;
	/**
	* Method change loacation on air and delete old location
	* @parametr[in] loc location to change
	*/
	void change_on_air( const location &loc );
		/**
	* Method change loacation on mud and delete old location
	* @parametr[in] loc location to change
	*/
	void change_on_mud( const location &loc );
	CObject *** matrix;
	SDL_Renderer * m_renderer;
	bool is_fill;
	CSurface container_of_surface;
};

