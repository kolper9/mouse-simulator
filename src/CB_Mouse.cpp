#include <string>
#include <SDL2/SDL.h> 
#include "location.h"
#include "CB_Mouse.h"
#include "constants.h"

std::string CB_Mouse::save(){
	return std::to_string(MOUSE)+" "+std::to_string(m_lives)+" "+ std::to_string(m_experiens);
}
	
 void CB_Mouse::draw_me( SDL_Renderer * renderer ){
	rect->x=m_x; 
	rect->y=m_y; 
	rect->w=m_wide; 
	rect->h=m_height;
	texture=SDL_CreateTextureFromSurface(renderer,SurfaceWithPicture);
	SDL_RenderCopy(renderer, texture ,NULL,rect);
	SDL_DestroyTexture(texture);
}
 
bool CB_Mouse::to_kill(){
	return true;
}
bool CB_Mouse::player(){
	return true;
}
CB_Mouse::CB_Mouse(){
	rect = new SDL_Rect;
	SurfaceWithPicture= SDL_LoadBMP( SMOUSE_IMG );
	m_wide=STANDART_O_WIDE;
	m_height=STANDART_O_HEIGHT;
	m_y=GROUND-m_height;
	m_x=0;
	m_lives=3;
	m_experiens=0;
	save_number=0;
}
CB_Mouse::CB_Mouse( int x, int y,  int lives, int xp ){
	rect = new SDL_Rect;
	SurfaceWithPicture= SDL_LoadBMP( SMOUSE_IMG );
	m_wide=STANDART_O_WIDE;
	m_height=STANDART_O_HEIGHT;
	m_y=y;
	m_x=x;
	m_lives=lives;
	m_experiens=xp;
	save_number=0;
}