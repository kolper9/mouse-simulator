#pragma once
/** Class load pictures of surfaces from file and give it to objects */
class CSurface
{
public:
	CSurface();
	~CSurface();
	/**
	*Method return surface with picture of object load from file
	*@param[in] code_of_object code of object is defined in constants.h
	*@return surface with picture or NULL when code_of_object is not define
	*/
	SDL_Surface * return_surface_of( const int code_of_object) const;
private:
	SDL_Surface * surface_of_chees;
	SDL_Surface * surface_of_grass;
	SDL_Surface * surface_of_posion;
	SDL_Surface * surface_of_mud;
	SDL_Surface * surface_of_air;
	SDL_Surface * surface_of_rock;
};

