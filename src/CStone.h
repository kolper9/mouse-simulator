#pragma once
#include "CObject.h"
#include <SDL2/SDL.h> 
#include <string>
/** Class CB_Spider represent enemy in game. It is control by driver.
* Main properties: can not go through */
class CStone : public CObject 
{
public:
		/**
	*Constructor of class 
	* @param[in] x place for spawn object
  * @param[in] y place for spawn object
  * @param[in] sufrace with object picture
  */
	CStone( int x, int y, SDL_Surface * surface );
		/** retun true if is posiible g trought this
		*@return
  */
	virtual bool can_go_trought();
		/**
  	* Method return information about object.
  	@return string type of object (2)
  	*/	
	virtual std::string save();
};
