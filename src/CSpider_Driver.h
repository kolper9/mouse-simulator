#pragma once
#include "CMAP.h"
#include <vector>
#include "location.h"
#include "CDriver.h"
/** Class represent driver of enemy spider */
class CSpider_Driver : public CDriver 
{
public:
	 /** Constructor of CDdriver setup driver for first use
	  * @param[in,out] Map for moving object
 	  * @param[in,out] object_to_control this object is driven by Driver
      */
	CSpider_Driver( CMAP * Map, CLive_Object * object_to_control  );
	/**
 	 * Method is call before live_object is move. In this metod should be called other method like attack... 
   	 */
	virtual void spec_funtion_for_type( const std::vector<location> &loc );
		/**
 		 * Method result what do with object on loc
 		 * @param[in] location for work 
   		*/
	virtual void eat_cube( const std::vector<location> &loc );
	 /**
  * Method move down object if under object is free space (AIR )
  * @return hight of fall 
   */
	int  gravitate();
	~CSpider_Driver();
};

