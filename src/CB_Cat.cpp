
#include <string>
#include <SDL2/SDL.h> 
#include "location.h"
#include "CB_Cat.h"
#include "constants.h"

std::string CB_Cat::save(){
	return std::to_string(CAT)+" "+std::to_string(m_lives)+" "+ std::to_string(m_experiens);
}

void CB_Cat::draw_me( SDL_Renderer * renderer ){
	rect->x=m_x; 
	rect->y=m_y; 
	rect->w=m_wide; 
	rect->h=m_height; 
	texture =SDL_CreateTextureFromSurface(renderer,SurfaceWithPicture);
	SDL_RenderCopy(renderer,texture ,NULL,rect);
	SDL_DestroyTexture(texture);
	m_experiens=0;

}	
bool CB_Cat::player(){
	return false;
}

bool CB_Cat::to_kill(){
 	return true;
}
CB_Cat::CB_Cat(){
	rect = new SDL_Rect;
	SurfaceWithPicture= SDL_LoadBMP(CAT_IMG);
	m_wide=STANDART_O_WIDE*3;
	m_height=STANDART_O_HEIGHT*2;
	m_x=WIN_WIDE-STANDART_O_WIDE*5;
	m_y=GROUND-m_height;
	m_lives=1;
	save_number=0;
}
CB_Cat::CB_Cat( int x, int y,  int lives, int xp ){
	rect = new SDL_Rect;
	SurfaceWithPicture= SDL_LoadBMP(CAT_IMG);
	m_wide=STANDART_O_WIDE*3;
	m_height=STANDART_O_HEIGHT*2;
	m_x=x;
	m_y=y;
	m_lives=lives;
	save_number=0;
	m_experiens=xp;
}
