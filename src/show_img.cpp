#include "show_img.h"
#include <SDL2/SDL.h> 
void show_img(SDL_Renderer * renderer, const char * name, int x, int y, int height, int wide){
	SDL_Rect * rect;
	rect = new SDL_Rect;
	rect->x = x; 
	rect->y = y; 
	rect->w = wide; 
	rect->h = height; 
	SDL_Surface * SurfaceWithPicture = SDL_LoadBMP( name );
	SDL_Texture * texture=SDL_CreateTextureFromSurface( renderer, SurfaceWithPicture );
	SDL_RenderCopy( renderer, texture, NULL, rect );
	SDL_DestroyTexture( texture );
	SDL_RenderPresent( renderer );
	delete rect;
	SDL_FreeSurface( SurfaceWithPicture );
}