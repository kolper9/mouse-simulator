#pragma once
#include "CDriver.h"
/** abstract class for generator od moves send signal to driver */
class CGenerator_moves{

	public:
	/**
  * Methods generate move and control driver.
   */
	virtual void generate_move()=0;	

	virtual ~CGenerator_moves(){}
	protected:

		CDriver * m_driver;
		bool side;
};