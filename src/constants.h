#pragma once

#define CHEES_IMG "src/img/chees.bmp"
#define GRASS_IMG "src/img/grass.bmp"
#define POSION_IMG "src/img/posion.bmp"
#define BMOUSE_IMG "src/img/mouse.bmp"
#define SMOUSE_IMG "src/img/mouse1.bmp"
#define EMOUSE_IMG "src/img/mouse2.bmp"
#define LIVE_IMG "src/img/live.bmp"
#define DEATH_IMG "src/img/live_dead.bmp"
#define CAT_IMG    "src/img/cat.bmp"
#define AIR_IMG  "src/img/air.bmp"
#define ROCK_IMG  "src/img/rock.bmp"
#define  MUD_IMG        "src/img/mud.bmp"
#define END_IMG  "src/img/end.bmp"
#define  START_IMG        "src/img/start.bmp"
#define PUNCH_L_IMG "src/img/punch_left.bmp"
#define PUNCH_R_IMG "src/img/punch_right.bmp"
#define SPIDER_IMG "src/img/spider.bmp"
#define FONT       "src/font/BAUH.TTF"
#define TIME_OF_MINING 500
#define AIR           0
#define MUD           1
#define ROCK           2
#define CHEES 			3
#define POSION         	4
#define GRASS           5
#define CAT           6
#define MOUSE          7
#define SPIDER 			8
#define RIGHT  0
#define LEFT  1
#define FREE_MOVE 1
#define CAN_GO_THROUGT 0
#define TIME_OF_FIGHT 300
#define TIME_OF_PUNCH 50
#define STANDART_O_WIDE    10	// SETUP GAME
#define STANDART_O_HEIGHT   10	// SETUP GAME
#define WIN_WIDE      800 // SETUP GAME
#define WIN_HEIGHT     600	// SETUP GAME
#define MAX_FPS 30			// SETUP GAME
#define FILE_FOR_SAVE_GAME	"SAVE_GAME"		// SETUP GAME
const int GROUND=(WIN_HEIGHT/5/STANDART_O_HEIGHT)*STANDART_O_HEIGHT;


