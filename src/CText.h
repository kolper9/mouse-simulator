#pragma once 
#include <string>
#include <SDL2/SDL.h> 
#include <SDL2/SDL_ttf.h>
#include "location.h"
#include "CB_Mouse.h"
#include "constants.h"
#include <string>
/** This class is use for print text on window */
class CText
{
public:
	/**
	*@throw const char* Font error 
	*/
	CText();
	 /**
  * Method print text to renderer
  * @param[in] text to show
  * @param[in,out] renderer where will be print a message 
  * @param[in] x place for text
  * @param[in] y place for text
  * @param[in] wide of text
  * @param[in] height of text
   */
	void show( std::string &text, SDL_Renderer * renderer, int x, int y, int wide, int height );
	~CText(); 	
private:
	SDL_Rect * rect;
	SDL_Texture * texture;
	SDL_Color color;
	SDL_Surface *text_surface;
	TTF_Font *font;
};
