#include <SDL2/SDL.h> 
#include "CObject.h"
#include "CLive.h"
#include "constants.h"

void CLive::change_on_death(){
	SDL_FreeSurface(SurfaceWithPicture);
	SurfaceWithPicture= SDL_LoadBMP( DEATH_IMG );
}
void CLive::change_on_live(){
	SDL_FreeSurface(SurfaceWithPicture);
	SurfaceWithPicture= SDL_LoadBMP( LIVE_IMG );
}

CLive::CLive( int x, int y ){
	m_y=y;
	m_x=x;
	rect = new SDL_Rect;
	SurfaceWithPicture= SDL_LoadBMP( LIVE_IMG );
	m_wide=STANDART_O_WIDE;
	m_height=STANDART_O_HEIGHT;
}

CLive::~CLive(){
if( rect ){
		delete rect;
		rect=NULL;
	}	
SDL_FreeSurface(SurfaceWithPicture);
}