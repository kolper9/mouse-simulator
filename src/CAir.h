#pragma once
#include "CObject.h"
#include <SDL2/SDL.h> 
#include <string>
/** Class CAir represents box object in game 
* Main properties: is free_move
*/
class CAir : public CObject 
{
public:
		/**
	*Constructor of class 
	* @param[in] x place for spawn object
  * @param[in] y place for spawn object
  * @param[in] sufrace with object picture
  */
	CAir( int x, int y, SDL_Surface * surface);
		/**
 	*@return  true if can go trought
  */
	bool free_move();
		/**
  	* Method return information about object.
  	@return string type of object (0)
  	*/	
	virtual std::string save();

};
