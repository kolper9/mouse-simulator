#include <string>
#include <SDL2/SDL.h> 
#include "location.h"
#include "CB_Spider.h"
#include "constants.h"
std::string CB_Spider::save(){
	return std::to_string(SPIDER)+" "+std::to_string(m_lives)+" "+ std::to_string(m_experiens);
}

void CB_Spider::draw_me( SDL_Renderer * renderer ){
	rect->x=m_x; 
	rect->y=m_y; 
	rect->w=m_wide; 
	rect->h=m_height; 
	texture =SDL_CreateTextureFromSurface(renderer,SurfaceWithPicture);
	SDL_RenderCopy(renderer,texture ,NULL,rect);
	SDL_DestroyTexture(texture);
	m_experiens=0;

}	


bool CB_Spider::to_kill(){
 	return true;
}
bool CB_Spider::player(){
	return false;
}
CB_Spider::CB_Spider(){
	rect = new SDL_Rect;
	SurfaceWithPicture= SDL_LoadBMP(SPIDER_IMG);
	m_wide=STANDART_O_WIDE;
	m_height=STANDART_O_HEIGHT;
	m_x=WIN_WIDE-STANDART_O_WIDE*5;
	m_y=GROUND+m_height*(rand()%20);
	m_lives=1;
	save_number=0;
}
CB_Spider::CB_Spider( int x, int y,  int lives, int xp ){
	rect = new SDL_Rect;
	SurfaceWithPicture= SDL_LoadBMP(SPIDER_IMG);
	m_wide=STANDART_O_WIDE;
	m_height=STANDART_O_HEIGHT;
	m_x=x;
	m_y=y;
	m_lives=lives;
	save_number=0;
	m_experiens=xp;
}



