
#include <SDL2/SDL.h> 
#include <SDL2/SDL_ttf.h>
#include <vector>  
#include <time.h> 
#include <string>
#include <fstream>

#include "location.h"
#include "CCat_Driver.h"
#include "constants.h"
#include "CCore.h"
#include "location.h"
#include "CStone.h"
#include "CAir.h"
#include "CMud.h"
#include "CPosion.h"
#include "CChees.h"
#include "CGrass.h"
#include "CDriver.h"
#include "CB_Mouse.h"
#include "CB_Cat.h"
#include "CB_Spider.h"
#include "CGrafic.h"
#include "CSpider_Driver.h"
#include "CSpider_generator_moves.h"
#include "CCat_generator_moves.h"


void   CCore::load_map( const char * file_name ){
std::ifstream      fi (  file_name, std::ios::binary | std::ios::in ); 
char c;
int x=0; 
int y=0;
int tmp_x=0;
bool player_find=false;
bool load=false;


while(fi>>c){
	switch(c){
		case '0': {
				Map->change_on(location(x,y),new CAir(x,y,container_of_surface.return_surface_of(AIR)));
				Map->draw_me(location(x,y));
				load=true;
				break;
			}
		case '1':{
				Map->change_on(location(x,y),new CMud(x,y,container_of_surface.return_surface_of(MUD)));
				Map->draw_me(location(x,y));
				load=true;
				break;
		} 
		case '2':{ 
			Map->change_on(location(x,y),new CStone(x,y,container_of_surface.return_surface_of(ROCK)));
			Map->draw_me(location(x,y));
			load=true;

			break;
		}
		case '3':{
			Map->change_on(location(x,y),new CChees(x,y,container_of_surface.return_surface_of(CHEES)));
			Map->draw_me(location(x,y));
			load=true;
			break;
		}
		case '4':{
			Map->change_on(location(x,y),new CPosion(x,y,container_of_surface.return_surface_of(POSION)));
			Map->draw_me(location(x,y));
			load=true;
			break;
		}
		case '5':{
			Map->change_on(location(x,y),new CGrass(x,y,container_of_surface.return_surface_of(GRASS)));
			Map->draw_me(location(x,y));
			load=true;
			break;
		}
			case '6':{
			int xp,lives;
			fi>>lives;
			fi>>xp;
			CB_Cat * tmp_cat=new CB_Cat(x,y,lives,xp);
			Map->change_on(location(x,y),tmp_cat);
			CDriver *  driver=new CCat_Driver(Map,tmp_cat);
			enemies.push_back(std::make_pair(new CCat_generator_moves (driver),driver));
			Map->draw_me(location(x,y));
			load=true;							
			break;
		}
		case '7':{
			int xp,lives;
			fi>>lives;
			fi>>xp;
			if(!player_find){
				player_find=true;
			}
			else{
				throw "BAD_FILE_ERROR";
			}

			CB_Mouse * tmp_mouse= new CB_Mouse(x,y,lives,xp);
			Map->change_on(location(x,y),tmp_mouse);
			Map->draw_me(location(x,y));
			mouse_driver= new CMouse_Driver (Map, tmp_mouse);
			
			load=true;
			
			
				break;
		}
		case '8':{
			int xp,lives;
			fi>>lives;
			fi>>xp;

			CB_Spider * tmp_spider=new CB_Spider(x,y,lives,xp);
			Map->change_on(location(x,y),tmp_spider);
			CDriver * driver=new CSpider_Driver(Map,tmp_spider);
			enemies.push_back(std::make_pair(new CSpider_generator_moves (driver),driver));
			Map->draw_me(location(x,y));
			load=true;
				break;
		}
		case ',':{
			x+=STANDART_O_WIDE;
			if(load==false){
				throw "BAD_FILE_ERROR";
			}
			else{
				load=false;	
			}
			break;
		}
		case '/':y+=STANDART_O_HEIGHT;tmp_x=x;x=0;break;
	}
}
if(tmp_x!=WIN_WIDE || WIN_HEIGHT!=y ){
	throw "BAD_FILE_ERROR";
}
fi.close();
}


void CCore::add_enemy_spider(){


	CSpider_Driver * spider_driver= new CSpider_Driver(Map, new CB_Spider());
	
	enemies.push_back(std::make_pair(new CSpider_generator_moves (spider_driver),spider_driver));
}
void CCore::update_scene(){
if(enemies.size()==0){
	add_enemy();	
}

for(auto i=enemies.begin();i!=enemies.end();i++){

	if(!i->second->is_dead()){
		i->first->generate_move();
		i->second->draw_me(grafic.get_renderer());		
	}
	else{
		i->second->draw_me(grafic.get_renderer());
		if(!(rand()%100)){
			delete i->first;
			delete i->second;
			enemies.erase(i);
			add_enemy();
		}	
	}
}


mouse_driver->draw_me(grafic.get_renderer());
	if(mouse_driver->is_dead()){
		grafic.draw_all();
		SDL_Delay(1000);
		grafic.death_screen();
		program_run=false;
	}	
}


void CCore::add_player(){
	 mouse_driver= new CMouse_Driver (Map, new CB_Mouse);
}
void  CCore::input_control( CMouse_Driver &object_to_control){
while(SDL_PollEvent(event))
{
    if (event->type == SDL_QUIT){
    	program_run=false;
		return;
	}          
   	if (event->type == SDL_KEYDOWN){
		switch(event->key.keysym.sym){
			case SDLK_RIGHT:{
					object_to_control.move_right();
			}
			break;
			case SDLK_LEFT:{
				object_to_control.move_left();
			}
			break;
			case SDLK_UP:{
				object_to_control.move_up();
			}
			break;
			case SDLK_DOWN:{
				object_to_control.move_down();
			}
			break;
			case SDLK_SPACE:{
				object_to_control.change_type_of_put(MUD);
			}
			break;
			case SDLK_LCTRL:{
				object_to_control.attack_prep();
			}
			break;
			case SDLK_s:{
				object_to_control.save();
			}
			break;
		}			
	}
}   
	state = SDL_GetKeyboardState(NULL);
	if(!(state[	SDL_SCANCODE_RIGHT] ||  state[SDL_SCANCODE_DOWN] || state[SDL_SCANCODE_LEFT] || state[SDL_SCANCODE_UP])){
		object_to_control.reset_timer();
	}     
}


CCore::CCore(){

	if(WIN_WIDE<200 || WIN_HEIGHT<200 || (STANDART_O_HEIGHT%10) || (STANDART_O_WIDE%10) || STANDART_O_WIDE<10 || STANDART_O_HEIGHT<10 
		|| WIN_WIDE%10 || WIN_HEIGHT%10  ){
		throw "SETTING ERROR";
	}

	program_run=true;
	event = new SDL_Event ;
	cat=NULL;
	mouse_driver=NULL;
	Map=NULL;

	if(SDL_Init (SDL_INIT_VIDEO|SDL_INIT_TIMER)==-1){
		throw "SDL_LIBRARY_ERROR" ;  
	}
	srand (time(NULL));
	if(TTF_Init()==-1) {
	  throw "TTF_LIBRARY_ERROR" ;  
	}
}


void CCore::add_enemy(){
	if(!(rand()%2)){
		add_enemy_cat();
	}
	else{
		add_enemy_spider();
	}
}

void CCore::add_enemy_cat(){

	cat=new CB_Cat();
	cat_driver= new CCat_Driver(Map, cat);
	generator= new CCat_generator_moves (cat_driver);
	enemies.push_back(std::make_pair(generator,cat_driver));
}

void CCore::init( const char * file_name ){
	
		grafic.creat_window();
		grafic.clear();
		grafic.welcom_screen();
		if(!file_name){
			 Map= new CMAP (grafic.get_renderer());
	 		 Map->first_use();
	 		 add_player();
			 add_enemy();
			 add_enemy();
		}
		else{
			Map= new CMAP (grafic.get_renderer());
			load_map(file_name);

	
	}
}

void CCore::main_loop(){
	int tim;
	grafic.draw_all();
	while(program_run){
		tim=SDL_GetTicks();

		input_control(*mouse_driver);

		update_scene();	

		grafic.draw_all();

		if(SDL_GetTicks()-tim<1000/MAX_FPS){ 
			SDL_Delay(1000/MAX_FPS-(SDL_GetTicks()-tim));
		}
	}
}
CCore::~CCore(){
	SDL_QuitSubSystem(SDL_INIT_VIDEO|SDL_INIT_TIMER);
	SDL_Quit();	
	if(Map){
		delete Map;
	}
	
	if(mouse_driver){
		delete mouse_driver;
	}
	delete event;
	TTF_Quit();
}