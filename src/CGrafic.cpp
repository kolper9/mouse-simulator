#include <SDL2/SDL.h> 
#include "CGrafic.h"
#include "constants.h"
#include "show_img.h"
void CGrafic::welcom_screen(){
	show_img( renderer, START_IMG, 0, 0, WIN_HEIGHT, WIN_WIDE);
	while(SDL_GetTicks()<2000){
		SDL_Delay(10);
	}
}
void CGrafic::death_screen(){
	show_img( renderer, END_IMG, 0, 0, WIN_HEIGHT, WIN_WIDE);
	SDL_Delay(2000);
}

void CGrafic::creat_window(){
	is_alock=true;
	win= SDL_CreateWindow("MOUSE SIMULATOR",SDL_WINDOWPOS_CENTERED,SDL_WINDOWPOS_CENTERED,WIN_WIDE,WIN_HEIGHT,SDL_WINDOW_MINIMIZED );
	if (win == NULL) {
       throw "WINDOW_CREAT_ERROR";
    }
	renderer=SDL_CreateRenderer(win,-1,0); 
}
void CGrafic::clear(){
	SDL_SetRenderDrawColor(renderer,0,0,0,255);
	SDL_RenderClear(renderer);
	SDL_RenderPresent(renderer);
}
CGrafic::~CGrafic(){	
	if(is_alock){
			SDL_DestroyRenderer(renderer);
			SDL_DestroyWindow(win);
	}
}
CGrafic::CGrafic(){
	is_alock=false;
}
void CGrafic::draw_all(){
	SDL_RenderPresent(renderer);
}

SDL_Renderer * CGrafic::get_renderer(){
	return renderer;
}
