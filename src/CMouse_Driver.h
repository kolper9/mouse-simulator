#pragma once 
#include <vector>
#include <SDL2/SDL.h> 
#include "CText.h"
#include "CHealthBar.h"
#include "location.h"
#include "CDriver.h"
#include "CLive_Object.h"
#include "CMAP.h"
/** Class represents  driver for mouse, drive objects and coordinate their moves and properties with map */
class CMouse_Driver : public CDriver 
{
public:
	CMouse_Driver( CMAP * Map, CLive_Object * object_to_control  );
	void attack_prep();
	virtual void spec_funtion_for_type( const std::vector<location> &loc );
	virtual void eat_cube( const std::vector<location> &loc );
	virtual void draw_me( SDL_Renderer * renderer );
	virtual void save();
private:
	void show_puch(const char * img, int wide, SDL_Renderer * renderer );
	void draw_xp( SDL_Renderer * renderer );
	bool punchin_right;
	bool punchin_left;
	CText text;
	CHealthBar Hbar;
};
