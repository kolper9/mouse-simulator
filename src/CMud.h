#pragma once
#include "CObject.h"
#include <SDL2/SDL.h> 
#include <string>
/** Class CB_Spider represent enemy in game. It is control by driver.
* Main properties: non */
class CMud : public CObject 
{
public:
	/**
	*Constructor of class 
	* @param[in] x place for spawn object
  * @param[in] y place for spawn object
  * @param[in] sufrace with object picture
  */
	CMud( int x, int y, SDL_Surface * surface );
		/**
  	* Method return information about object.
  	@return string type of object (1)
  	*/	
	virtual std::string save();
};

