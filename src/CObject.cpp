
#include <SDL2/SDL.h> 
#include "CObject.h"
#include <string>
#include "constants.h"
bool CObject::save_able(){
	return true;
}
std:: string CObject::save(){
	return std:: to_string(AIR);
}
CObject::~CObject(){
	if( rect ){
		delete rect;
		rect=NULL;
	}	
}
void CObject::draw_me( SDL_Renderer * renderer ){
	rect->x=m_x; 
	rect->y=m_y; 
	rect->w=m_wide; 
	rect->h=m_height; 
	texture=SDL_CreateTextureFromSurface( renderer, SurfaceWithPicture );
	SDL_RenderCopy( renderer, texture, NULL, rect );
	SDL_DestroyTexture( texture );
}
bool CObject::deletable(){
	return true;
}
bool CObject::free_move(){
	return false;
}
bool CObject::can_go_trought(){
	return true;
}
int CObject::live_change(){
	return 0;
}
bool CObject::to_kill(){
	return false;
}