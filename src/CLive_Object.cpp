#include "CObject.h"
#include "CLive_Object.h"
#include <string>
#include <SDL2/SDL.h> 
#include "constants.h"
#include <vector> 
#include "location.h"

bool CLive_Object::save_able(){
	int tmp=save_number;
	save_number=(save_number+1)%(m_height/STANDART_O_HEIGHT*m_wide/STANDART_O_WIDE);
	return !tmp;
}
bool CLive_Object::is_kill(){
	return killed;
}
 int CLive_Object::height(){
 	return m_height;
 }
int CLive_Object::wide(){
	return m_wide;
}

int CLive_Object::change_live( int lives ){
	m_lives+=lives;
	if(m_lives>3){
		m_lives=3;
	}
	if(m_lives==0){
		killed=true;
	}
	return m_lives;
}
CLive_Object::CLive_Object(){
}
//--------------------------------------------------------------------------------------------------------------------------------------------
int CLive_Object::M_X(){
	return m_x;
}
//--------------------------------------------------------------------------------------------------------------------------------------------
int CLive_Object::M_Y(){
	return m_y;
}
//--------------------------------------------------------------------------------------------------------------------------------------------
bool CLive_Object::deletable(){
	return false;
}
bool CLive_Object::free_move(){
	return false;
}
//--------------------------------------------------------------------------------------------------------------------------------------------
bool CLive_Object::can_go_trought(){
		return false;
}
//--------------------------------------------------------------------------------------------------------------------------------------------
std::vector<location> CLive_Object::top() const{
	std::vector<location>	loc;
	for(int i=m_x;i<m_x+m_wide;i+=STANDART_O_WIDE ){
		int j= m_y-STANDART_O_HEIGHT; 
		loc.push_back(location(i,j));		
	}
	return loc;
}
//--------------------------------------------------------------------------------------------------------------------------------------------
std::vector<location> CLive_Object::front() const{
	std::vector<location>	loc;
	int i=m_x+m_wide;
	for(int j= m_y; j<m_y+m_height;j+=STANDART_O_HEIGHT){
		loc.push_back(location(i,j));
	}
	return loc;
}
//--------------------------------------------------------------------------------------------------------------------------------------------
std::vector<location> CLive_Object::back() const{
	std::vector<location>	loc;
	int i=m_x-STANDART_O_WIDE;
	for(int j= m_y; j<m_y+m_height;j+=STANDART_O_HEIGHT){
		loc.push_back(location(i,j));
	}
	return loc;
}
//--------------------------------------------------------------------------------------------------------------------------------------------
std::vector<location> CLive_Object::under() const{
	std::vector<location>	loc;
	for(int i=m_x;i<m_x+m_wide;i+=STANDART_O_WIDE ){
		int j= m_y+m_height;
		loc.push_back(location(i,j));	
	}
	return loc;
}
//--------------------------------------------------------------------------------------------------------------------------------------------
std::vector<location>  CLive_Object::location_for_faling() const{
 	std::vector<location>	loc;
	for(int i=m_x-STANDART_O_WIDE;i<m_x+m_wide+STANDART_O_WIDE;i+=STANDART_O_WIDE ){
		int j= m_y+m_height;
		loc.push_back(location(i,j));	
	}
	return loc;
 }
 //--------------------------------------------------------------------------------------------------------------------------------------------
std::vector<location> CLive_Object::locations_of_object()const{
	std::vector<location>	loc;
	for(int i=m_x;i<m_x+m_wide;i+=STANDART_O_WIDE ){
		for(int j= m_y; j<m_y+m_height;j+=STANDART_O_HEIGHT){
			loc.push_back(location(i,j));	
		}
	}
	return loc;
}
//--------------------------------------------------------------------------------------------------------------------------------------------
void CLive_Object::move_left(){	
	m_x-=STANDART_O_WIDE;
}

//--------------------------------------------------------------------------------------------------------------------------------------------
void CLive_Object::move_right(){
	m_x+=STANDART_O_WIDE;
}
//--------------------------------------------------------------------------------------------------------------------------------------------
void CLive_Object::move_up(){
	m_y-=STANDART_O_HEIGHT;
}
//--------------------------------------------------------------------------------------------------------------------------------------------
void CLive_Object::move_down(){
	m_y+=STANDART_O_HEIGHT;
}
//--------------------------------------------------------------------------------------------------------------------------------------------
int CLive_Object::xpincrease( int xp ){
	m_experiens+=xp;
	return m_experiens;
}