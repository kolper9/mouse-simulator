

#include <SDL2/SDL.h> 
#include "CHealthBar.h"
#include "constants.h"
#include "CLive.h"


void CHealthBar::re_draw(  SDL_Renderer * renderer, int lives ){
	/*if(m_lives==lives){
		return;
	}
	else{
		m_lives=lives;
	}*/
	if( lives == 0 ){
		first->change_on_death();
		second->change_on_death();
		third->change_on_death();
	}
	if( lives == 1 ){
		first->change_on_live();
		second->change_on_death();
		third->change_on_death();	
	}
	if( lives == 2 ){
		first->change_on_live();
		second->change_on_live();
		third->change_on_death();
	}
	if( lives == 3 ){
		first->change_on_live();
		second->change_on_live();
		third->change_on_live();
	}
	first->draw_me(renderer);
	second->draw_me(renderer);
	third->draw_me(renderer);
}
 
CHealthBar::~CHealthBar(){
delete first;
delete second;
delete third;	
}
CHealthBar::CHealthBar(){
	first=new CLive( x, y );
	second=new CLive( x-STANDART_O_WIDE, y );
	third=new CLive( x-2*STANDART_O_WIDE, y );
	m_lives=0;
}