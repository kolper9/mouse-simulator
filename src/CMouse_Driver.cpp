#include <string>
#include <fstream>
#include "constants.h"
#include "CText.h"
#include "CMouse_Driver.h"
#include "CMAP.h"
#include "show_img.h"

void CMouse_Driver::save(){
	std::ofstream      fo ( FILE_FOR_SAVE_GAME, std::ios::binary | std::ios::out ); 
	fo<<m_Map->save();
	fo.close();
}

void CMouse_Driver::draw_xp( SDL_Renderer * renderer ){
	for (int i=5*STANDART_O_WIDE;i<16*STANDART_O_WIDE;i+=STANDART_O_WIDE){
		for (int j=0;j<2*STANDART_O_HEIGHT;j+=STANDART_O_WIDE){
				m_Map->draw_me(location(WIN_WIDE-i,j));		
		}
	}
	std::string score="SCORE: "+std::to_string(m_object_to_control->xpincrease(0));
	text.show( score, renderer, WIN_WIDE-15*STANDART_O_HEIGHT,0, score.size()*STANDART_O_WIDE,2*STANDART_O_HEIGHT);
}
void CMouse_Driver::show_puch(const char * img, int wide , SDL_Renderer * renderer ){
	show_img( renderer, img , m_object_to_control->M_X()+wide, m_object_to_control->M_Y(), STANDART_O_HEIGHT ,  STANDART_O_WIDE);
	SDL_Delay(TIME_OF_PUNCH);
	m_Map->draw_me(location(m_object_to_control->M_X()+wide, m_object_to_control->M_Y()));
	punchin_left=false;
	punchin_right=false;
}

CMouse_Driver::CMouse_Driver( CMAP * Map, CLive_Object * object_to_control  ){
	m_Map=Map;
	m_object_to_control=object_to_control;
	m_code_of_object=0;
	is_waiting=false;
	time_by_wait=0;
	punchin_right=false;
 	punchin_left=false;
 	m_Map->change_on(m_object_to_control->locations_of_object(),m_object_to_control);
}

void CMouse_Driver::spec_funtion_for_type( const std::vector<location> &loc ){
	(void)loc;
}
void CMouse_Driver::eat_cube( const std::vector<location> &loc ){
	int lives=m_Map->health( loc );
	if( lives ){
		m_object_to_control->change_live( lives );
		m_object_to_control->xpincrease(10*lives);
	}	
}

void CMouse_Driver::attack_prep(){
	if(side==RIGHT){
		punchin_right=true; 
		attack(m_object_to_control->front());
	}
	if(side==LEFT){
		punchin_left=true;
		attack(m_object_to_control->back());
	}
}

void CMouse_Driver::draw_me( SDL_Renderer * renderer ){
	if(m_object_to_control->is_kill()){
		Hbar.re_draw( renderer, m_object_to_control-> change_live(0) );
		int i=AIR;
		m_object_to_control->draw_me( renderer );
		m_Map->re_draw(m_object_to_control->locations_of_object(),i);	
	}
	else{
		gravitate();
		draw_xp( renderer );
		Hbar.re_draw( renderer, m_object_to_control-> change_live(0) );

		if(punchin_left){
			show_puch(PUNCH_L_IMG,-STANDART_O_WIDE,renderer);
		}
		if(punchin_right){
			show_puch(PUNCH_R_IMG,STANDART_O_WIDE,renderer);
		}
		m_object_to_control->draw_me( renderer );
	}
}
