#pragma once 
#include <SDL2/SDL.h> 
/** Class creat window and contrlol window, and print screen in window*/
class CGrafic{

	public:

		CGrafic();
		~CGrafic();
 /**
  * Method move down object if under object is free space (AIR ).
  */
		void creat_window();
	/**
  * Method clear window and setup backspace color of window on black.
   */
		void clear();
	/**
  * Method draw renderer on window.
   */
		void draw_all();
	/**
  * Method creat welcom screen and draw it on window.
   */
		void welcom_screen();
	/**
  * Method creat death screen and draw it on window.
   */
		void death_screen();
			/**
  * Method return renderer of window
  * @return return render if suces or NULL if fail.
   */
		SDL_Renderer * get_renderer();

	private:

		bool is_alock;
		int code_of_object;
		SDL_Window * win;
		SDL_Renderer * renderer;
};
