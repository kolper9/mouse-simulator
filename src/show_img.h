#pragma once
#include <SDL2/SDL.h> 
	/**
	*Constructor of class 
	* @param[in] renderer where will be show picture
	* @param[in] name if file with picture
	* @param[in] x place for spawn object
	* @param[in] y place for spawn object
	* @param[in] height of picture
	* @param[in] wide of picture
 	*/
void show_img(SDL_Renderer * renderer, const char * name, int x, int y, int height, int wide);