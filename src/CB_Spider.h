
#pragma once
#include <string>
#include <SDL2/SDL.h> 
#include "CLive_Object.h"
/** Class CB_Spider represents enemy in game. It is control by driver.
* Main properties: is anti-gravitete */
class CB_Spider : public CLive_Object{
public:
	CB_Spider( );
		/**
  	* Method draw object to renderer.
  * @param[in] x place for spawn object
  * @param[in] y place for spawn object
  * @param[in] lives of object
  * @param[in] xp of object
  	*/
	CB_Spider( int x, int y, int lives, int xp );
	/**
  	* @return true if object can be killed if not false.
  	*/
	bool to_kill();
	/**
  	* Method draw object to renderer.
  	@param[in,out] renderer where will be draw object.
  	*/
	virtual void draw_me( SDL_Renderer * renderer );
	/**
  	* Method return information about object.
  	@return string type of object (7), lives, expirience.
  	*/	
	virtual std::string save();
	/**
  	@return return if object is player ( false ).
  	*/
	virtual bool player();
};


