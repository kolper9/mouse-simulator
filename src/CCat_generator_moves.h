#pragma once
#include "CDriver.h"
#include "CGenerator_moves.h"
/** Class represent generator of enemy cat, can send signal to driver */
class CCat_generator_moves : public CGenerator_moves{

public:

	/**
  *  @param[in,out] driver, this driver will be driven by this generator.
   */
	CCat_generator_moves( CDriver * driver);
	/**
  * Methods generate move and control driver.
   */
	void generate_move();	
};
