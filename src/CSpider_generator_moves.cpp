
#include <string>
#include <SDL2/SDL.h> 
#include "location.h"
#include "CDriver.h"
#include "CSpider_generator_moves.h"

CSpider_generator_moves::CSpider_generator_moves( CDriver * driver){
	side=false;
	m_driver=driver;
}
void CSpider_generator_moves::generate_move(){

	if(!(rand()%100)){
		side=!side;
	}
	
	if(!(rand()%3)){
		if(!(rand()%2)){
			m_driver->move_down();
		}
		else{
			m_driver->move_up();	  
		}	

	}
	else{
		if( side ){
			m_driver->move_right();
		}
		else{
			m_driver->move_left();	  
		}	
	}
}

