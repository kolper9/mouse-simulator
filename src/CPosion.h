
#pragma once
#include "CObject.h"
#include <SDL2/SDL.h> 
#include <string>
#include <string>
/** Class CPosion represents enemy in game. It is control by driver.
* Main properties: remove live */
class CPosion : public CObject 
{
public:
	CPosion( int x, int y, SDL_Surface * surface );
	int live_change();
	/**
  	* Method return information about object.
  	@return string type of object (4)
  	*/	
	virtual std::string save();

};

