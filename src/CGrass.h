#pragma once
#include <string>
#include "CObject.h"
#include <SDL2/SDL.h>
/** Class  CGrass represents box object in game 
* Main properties: non
*/ 
class CGrass : public CObject 
{
public:
	/**
	*Constructor of class 
	* @param[in] x place for spawn object
  * @param[in] y place for spawn object
  * @param[in] sufrace with object picture
  */
	CGrass( int x, int y ,  SDL_Surface * surface);
	/**
  	* Method return information about object.
  	@return string type of object (5)
  	*/	
	virtual std::string save();
};
