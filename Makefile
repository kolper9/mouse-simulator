

all: 
	cd src; make ;
run: 
	cd src; make run;
clean: 
	cd src; make clean;
	
compile: 
	cd src; make ; mv ./kolarp32 ..
doc: 
	cd src;  make doc;
.PHONY: all run doc clean compile
