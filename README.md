# MOUSE Simulator

Simple C++ game with SDK library. 

![Printscreen](/examples/printscreen.PNG)

## About
Mouse Simulator simulates the life of a mouse. The mouse moves on a map composed of blocks of different properties. The mouse can move freely in blocks representing air. It moves slower on the other blocks or can't even go through them. The mouse is controlled by the keyboard. You can move with arrows, to place block push space bar, attack with left Ctrl. The game is saved by using the s key. The game is saved in the SAVE_GAME file. If you want to load a map file, just specify it as the first argument, during starting the game. There are enemies on the map. The mouse can fight with them. But they also can kill the mouse. Mouse can be also injured by falling or swallowing poison. Lives are increased by eating cheese.
## How to run 
To run game you must make sure that you have the latest version of SDK library.

To compile game type `make` in base folder of game.